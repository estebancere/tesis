/*
 * File:   programa_principal.c
 * Author: Andres Dassano
 *
 * Created on 8 de mayo de 2020, 9:14
 */


#include"Configuracion.h"
#include <xc.h>
#include <math.h>
#include "USART_lib.h"
#include "EEPROM.h"
#include "LCD_Libreria.h"
#include "I2C_Libreria.h"
#include "ds3231_funciones.h"
#include "funciones_adicionales.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Declaracion_Variables.h"


void main (void) {
    
    I2C_Init();
//    Envio_Hora();
//    Envio_Fecha();
    Configurar_Alarma(); //Configuramos alarma a las 0:00:00 hs
    LCD_Init();
    USART_Init(9600);    
    TRISCbits.RC0 = 1;
    Activamos_INT2(); //Activamos interrupcion por flanco utilizada para detectar cambio de dia con alarma del DS3231

    if (EEPROM_Lectura(200)==255) { //Esta condicion se da unicamente cuando acabamos de programar el PIC
        Limpiar_EEPROM (); 
        address_general = memoria_segun_dias(); //Escribimos la posicion de memoria segun el dia corriente
        EEPROM_Guardar(199,Decena_Dia_Actual()); //Guardamos la decena del dia actual en una posicion de memoria  
        EEPROM_Guardar(198,Unidad_Dia_Actual); //Guardamos la unidad del dia actual en una posicion de memoria 
    }
    else { //Esta condicion se da ccuando al PIC se le corta la alimentacion y vuelve
        Pot_acumulada_dia_ms = EEPROM_Lectura(202)<<8; //Leemos byte mas significativo de la potencia acumulada mensual
        Pot_acumulada_dia_ls = EEPROM_Lectura(203); //Leemos byte menos significativo de la potencia acumulada mensual
        Pot_acumulada_dia = Pot_acumulada_dia_ms | Pot_acumulada_dia_ls; //Armamos byte de potencia acumulada  
        address_general = memoria_segun_dias();  //Escribimos la posicion de memoria segun el dia corriente
        if (Cambio_Dia(EEPROM_Lectura(198),EEPROM_Lectura(199))) { //Si hay un cambio de dia
            if (Cambio_de_Mes (EEPROM_Lectura(199))) { //Si hay cambio de dia y cambio de mes
                address_general = 0; //Reseteamos la address_general ya que vamos a inicializar desde cero. No haria falta ya que usamos la funcion memoria_segun_dias(), pero funciona asi que lo dejamos
                Limpiar_EEPROM(); //Limpiamos EEPROM ya que arranca un mes nuevo y debemos sobreescribir los valores de memoria
                Pot_acumulada_dia = 0; //Reiniciamos potencia acumulada ya que empieza un nuevo dia
                INTCON3bits.INT2IF = 0;      //Apagamos bandera de interrupcion de RB2
                Apagar_Alarma(); 
            }   
            else { //Si no hay cambio de mes, pero si cambio de dia
                Pot_acumulada_dia = 0; //Reiniciamos la potencia acumulada diaria
                INTCON3bits.INT2IF = 0; //Apagamos bandera de interrupcion de RB2
                Apagar_Alarma();     
            }
        }
           
            EEPROM_Guardar(199,Decena_Dia_Actual());
            EEPROM_Guardar(198,Unidad_Dia_Actual());
            k = 0; //Apagamos bandera que inicio la INT2
    }
    

    while (1){  
        Leer_Fecha_Completa();
        Mostrar_Fecha();
        Leer_Hora();
        Mostrar_Hora();
        
        if (k == 1) { 
            if (Cambio_de_Mes (EEPROM_Lectura(199))) {
                address_general = memoria_segun_dias(); //Escribimos la posicion de memoria segun el dia corriente
                Limpiar_EEPROM();
                Pot_acumulada_dia  = 0;
            }
            else {  
                Pot_acumulada_dia  = 0; //Reiniciamos la potencia acumulada diaria
                address_general = memoria_segun_dias(); //Escribimos la posicion de memoria segun el dia corriente
                INTCON3bits.INT2IF = 0;      //Apagamos bandera de interrupcion de RB2
            }

            EEPROM_Guardar(199,Decena_Dia_Actual());
            EEPROM_Guardar(198,Unidad_Dia_Actual());
            k = 0;    
        }
        
        if (PORTCbits.RC1 == 1) {
            Pot_acumulada_dia = Pot_acumulada_dia + 100;//ESTA LINEA DEBE SER ELIMINADA
        }
///////////ESCRIBIMOS MEMORIA Y ENVIAMOS DATOS POR UART// ////////////////       
        Guardar_Datos (Pot_acumulada_dia,address_general);
        Crear_Cadena();
        USART_Cadena(Crear_Cadena());
        __delay_ms(500);
    }}


void __interrupt () ISR() {
    if(INT2IF){
        k = 1; //Flag para guardar la decena del dia en que estamos y chequear cambio de mes (en main)
        Pot_acumulada_dia  =  0;
        address_general = memoria_segun_dias();
        Apagar_Alarma();      
        INTCON3bits.INT2IF = 0;      //Apagamos bandera de interrupcion de RB2
    }
}




