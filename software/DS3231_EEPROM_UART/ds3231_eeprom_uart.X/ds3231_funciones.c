#include <xc.h>
#include "Configuracion.h"
#include "LCD_Libreria.h"
#include "I2C_Libreria.h"
#include <stdint.h>
#include "ds3231_funciones.h"
#include "funciones_adicionales.h"
#include <stdlib.h>

void Envio_Hora (void) {
    horas = ((set_hora[0] & 0x0F)<<4) + (set_hora[1] & 0x0F);
    minutos = ((set_hora[2] & 0x0F)<<4) + (set_hora[3] & 0x0F);
    segundos = ((set_hora[4] & 0x0F)<<4) + (set_hora[5] & 0x0F);
    I2C_Start();
    I2C_Tx(0b11010000);
    I2C_Tx(0b00000000);
    I2C_Tx(segundos);
    I2C_Tx(minutos);
    I2C_Tx(horas);
    I2C_Stop();   
}

void Envio_Fecha (void) {
    dia = ((set_fecha[0] & 0x0F)<<4) + (set_fecha[1] & 0x0F);
    mes = ((set_fecha[2] & 0x0F)<<4) + (set_fecha[3] & 0x0F);
    anio = ((set_fecha[4] & 0x0F)<<4) + (set_fecha[5] & 0x0F);
    I2C_Start();
    I2C_Tx(0b11010000);
    I2C_Tx(0b00000100);
    I2C_Tx(dia);
    I2C_Tx(mes);
    I2C_Tx(anio);
    I2C_Stop();   
}
   
void Leer_Hora (void) {
    I2C_Start();
    I2C_Tx(0b11010000);
    I2C_Tx(0b00000000);
    I2C_Restart(); 
    I2C_Tx(0b11010001);
    segundos = I2C_Rx();
    I2C_Ack();
    minutos = I2C_Rx();
    I2C_Ack();
    horas = I2C_Rx();
    I2C_Nack();
    I2C_Stop();    
}

void Leer_Fecha_Completa (void) {
    I2C_Start();
    I2C_Tx(0b11010000);
    I2C_Tx(0b00000100);
    I2C_Restart(); 
    I2C_Tx(0b11010001);
    dia = I2C_Rx();
    I2C_Ack();
    mes = I2C_Rx();
    I2C_Ack();
    anio = I2C_Rx();
    I2C_Nack();
    I2C_Stop();    
}

void Mostrar_Hora (void) {
    LCD_XY(0,3);
    LCD_Data((horas>>4) + 0x30);
    LCD_Data((horas & 0x0F) + 0x30);
    LCD_Data(':');
    LCD_Data((minutos>>4) + 0x30);
    LCD_Data((minutos & 0x0F) + 0x30);
    LCD_Data(':');
    LCD_Data((segundos>>4) + 0x30);
    LCD_Data((segundos & 0x0F) + 0x30);        
    __delay_ms(100);
}

void Mostrar_Fecha (void) {
    LCD_XY(1,3);
    LCD_Data((dia>>4) + 0x30);
    LCD_Data((dia & 0x0F) + 0x30);
    LCD_Data('-');
    LCD_Data((mes>>4) + 0x30);
    LCD_Data((mes & 0x0F) + 0x30);
    LCD_Data('-');
    LCD_Data((anio>>4) + 0x30);
    LCD_Data((anio & 0x0F) + 0x30);        
    __delay_ms(100);
}

void Configurar_Alarma (void) {
    //Set control bits
    I2C_Start();
    I2C_Tx(0b11010000); //envio direccion del dispositivo
    I2C_Tx(0xE); //envio posicion de memoria a escribir
    I2C_Tx(0b00000101);
    I2C_Tx(0b00000000);
    I2C_Stop(); 
    
    //Set alarm at 0:00:00 hs
    I2C_Start();
    I2C_Tx(0b11010000); //envio direccion del dispositivo
    I2C_Tx(0x7); //envio posicion de memoria a escribir
    I2C_Tx(0b0000000); //segundos = 0 (primer bit  AM1=1)
    I2C_Tx(0b00000000); //minutos = 0 (primer bit AM2=0)
    I2C_Tx(0b00000000); //hora = 0 (primer bit AM3=0)
    I2C_Tx(0b10000000); //fecha = 24 (DT/DY=0, AM4=1)
    I2C_Stop();      
}

void Apagar_Alarma (void) {
    I2C_Start();
    I2C_Tx(0b11010000); //envio direccion del dispositivo
    I2C_Tx(0xF); //envio posicion de memoria a escribir
    I2C_Tx(0b00000000); //pongo flag de alarma en 0
    I2C_Stop();  
}

int Dia_Actual (void) {
    char cadena0_dia[16];
    char cadena1_dia[16];
    I2C_Start();
    I2C_Tx(0b11010000);
    I2C_Tx(0b00000100);
    I2C_Restart(); 
    I2C_Tx(0b11010001);
    dia = I2C_Rx();
    I2C_Nack();
    I2C_Stop(); 
    
    
    itoa ((dia>>4), cadena0_dia, 10);
    itoa ((dia & 0x0F), cadena1_dia, 10);
    concatenate (cadena0_dia, cadena1_dia);
//    LCD_XY(3,0);
//    LCD_Cadena(cadena0_dia);
    int dia_int = atoi (cadena0_dia);
    return dia_int;
    //return cadena0_dia;
    
}

int Decena_Dia_Actual (void) {
    int decena_dia;
    I2C_Start();
    I2C_Tx(0b11010000);
    I2C_Tx(0b00000100);
    I2C_Restart(); 
    I2C_Tx(0b11010001);
    dia = I2C_Rx();
    I2C_Nack();
    I2C_Stop(); 
    
    decena_dia = (dia>>4);

    return decena_dia;
    
}

int Unidad_Dia_Actual (void) {
    int unidad_dia;
    I2C_Start();
    I2C_Tx(0b11010000);
    I2C_Tx(0b00000100);
    I2C_Restart(); 
    I2C_Tx(0b11010001);
    dia = I2C_Rx();
    I2C_Nack();
    I2C_Stop(); 
    
    unidad_dia = (dia & 0x0F);

    
    return unidad_dia;
    
}