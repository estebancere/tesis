#include <xc.h>
#include "Configuracion.h"
#include "LCD_Libreria.h"
#include "I2C_Libreria.h"
#include <stdint.h>

char set_hora [] = "235930";
char set_fecha [] = "310421";
void Envio_Hora (void);
void Envio_Fecha (void);
void Leer_Hora (void);
void Leer_Fecha_Completa (void);
void Mostrar_Hora (void);
void Mostrar_Fecha (void);
void Configurar_Alarma (void);
void Apagar_Alarma (void);
int Dia_Actual (void);
int Decena_Dia_Actual (void);
int Unidad_Dia_Actual (void);

uint8_t horas,minutos,segundos;
uint8_t dia,mes,anio;
