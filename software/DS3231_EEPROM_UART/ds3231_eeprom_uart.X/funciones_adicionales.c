#include"Configuracion.h"
#include <xc.h>
#include <math.h>
#include "USART_lib.h"
#include "EEPROM.h"
#include "LCD_Libreria.h"
#include "I2C_Libreria.h"
#include "ds3231_funciones.h"
#include "funciones_adicionales.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *itoa(int value, char *result, int base) { 
    // check that the base if valid
    if (base< 2||base>36) {*result = '\0'; return result; }
    char* ptr=result, *ptr1=result, tmp_char;
    int tmp_value;
do { 
tmp_value=value;
value/= base;
*ptr++="zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)]; } while ( value ); 

// Apply negative sign 
if (tmp_value<0) *ptr++ = '-';
    *ptr-- = '\0';
while (ptr1 < ptr) { 
tmp_char=*ptr; *ptr--= *ptr1; *ptr1++ = tmp_char;
} 
    return result; 
    }

void concatenate(char p[], char q[]) {
   int c, d;
   
   c = 0;
 
   while (p[c] != '\0') {
      c++;      
   }
 
   d = 0;
 
   while (q[d] != '\0') {
      p[c] = q[d];
      d++;
      c++;    
   }
 
   p[c] = '\0';
}

Guardar_Datos (int Pot_acumulada_dia_int, int address_int) {
    int address_diaria = 0;
    int digito;
    char Pot_acumulada_dia_ms_int;
    char Pot_acumulada_dia_ls_int;
 
    
    digito = Pot_acumulada_dia_int / 10000; // Se obtiene el bit m�s significativo de los 4 bits que forman ?V_medido?, esto es debido a que se divide por 1000 quedando un n�mero con tres decimales, pero al ser ?V_medido? un variable de tipo entero, se pierden los decimales
    EEPROM_Guardar(address_diaria+address_int, digito); 
    address_diaria++;
    EEPROM_Guardar(150,address_diaria);


    digito = (Pot_acumulada_dia_int / 1000) %10; // Se obtiene el bit m�s significativo de los 4 bits que forman ?V_medido?, esto es debido a que se divide por 1000 quedando un n�mero con tres decimales, pero al ser ?V_medido? un variable de tipo entero, se pierden los decimales
    EEPROM_Guardar(address_diaria+address_int, digito); 
    address_diaria++;
    EEPROM_Guardar(150,address_diaria);


    digito = (Pot_acumulada_dia_int / 100)%10;
    EEPROM_Guardar(address_diaria+address_int, digito); 
    
    
    address_diaria = 0;
    EEPROM_Guardar(150,address_diaria);

    
    Pot_acumulada_dia_ls_int = (char) 0x00FF & Pot_acumulada_dia_int;
    Pot_acumulada_dia_ms_int = (char) 0x00FF & (Pot_acumulada_dia_int >> 8);
    EEPROM_Guardar(202,Pot_acumulada_dia_ms_int);
    EEPROM_Guardar(203,Pot_acumulada_dia_ls_int);

}

char* Crear_Cadena (void) {
    
        char cadena0[250] = "S";
        char cadena1[20];


        for (int r=0;r<93;r++){
            int x = r;
            itoa (EEPROM_Lectura(r), cadena1, 10); 
            concatenate(cadena0,cadena1);

            if (r==1 | r==4 | r==7 | r==10 | r==13  | r==16 | r==19 | r==22 | r==25 | r==28 | r==31 | r==34 | r==37 | r==40 | r==43 | r==46 | r== 49 | r==52 | r==55 | r==58 | r==61 | r==64 | r==67 | r==70 | r==73 | r==76 | r==79 | r==82 | r==85 | r==88 | r==91) {
                concatenate(cadena0,".");
            }
            if (r==2 | r==5 | r==8 | r==11 | r==14 | r==17 | r==20 | r==23 | r==26 | r==29 | r==32 | r==35 | r==38 | r==41 | r==44 | r==47 | r==50 | r==53 | r==56 | r==59 | r==62 | r==65 | r==68 | r==71 | r==74 | r==77 | r==80 | r==83 | r==86 | r==89) {
                concatenate(cadena0,",");
            }
       
        }
        
        return cadena0;
}

int memoria_segun_dias (void) {
    int address_general_;
    address_general_ = ((Dia_Actual()-1)*3);
    return address_general_;
}

int Cambio_de_Mes (int Dia_Ayer_int) {
    int cambio_mes = 0;
    int Dia_Hoy_int;
    Dia_Hoy_int = Decena_Dia_Actual();
    //Dia_Ayer_int=EEPROM_Lectura(217);
    if (Dia_Ayer_int>Dia_Hoy_int){
        cambio_mes=1;
   }
    else{
        cambio_mes= 0;
     }

    //Dia_Ayer_int=Dia_Hoy_int ;
    //EEPROM_Guardar(199,25);
 
     return cambio_mes;
 }

int Cambio_Dia (int Unidad_Dia_Ayer_int,int Decena_Dia_Ayer_int) {
    int cambio_dia = 0;
    int Unidad_Dia_Hoy_int;
    int Decena_Dia_Hoy_int;
        Unidad_Dia_Hoy_int = Unidad_Dia_Actual();
        Decena_Dia_Hoy_int = Decena_Dia_Actual();
    //Dia_Ayer_int=EEPROM_Lectura(217);

   
    if ((Unidad_Dia_Hoy_int!=Unidad_Dia_Ayer_int) | (Unidad_Dia_Hoy_int==Unidad_Dia_Ayer_int & Decena_Dia_Hoy_int!=Decena_Dia_Ayer_int)){
        cambio_dia = 1;
        }
    else{
        cambio_dia = 0;
     }
 
     return cambio_dia;
}

void Encender_Led (void) {
        TRISCbits.RC2 =0;
        PORTCbits.RC2 = 1;
        __delay_ms(300);
        PORTCbits.RC2 = 0;

}

void Activamos_INT2 (void) {
    INTCONbits.GIE = 1;
    TRISBbits.RB2 = 1;
    INTCON3bits.INT2IE = 1; // Habilita interrupcion externa INT2
    INTCON2bits.INTEDG2 = 0; // Interrupcion con faling edge en INT2
    INTCON3bits.INT2IF = 0; // Bandera de interrupcion INT2 desactivada
    TRISCbits.RC1 = 1; 
}
