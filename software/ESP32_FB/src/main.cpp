#include <Arduino.h>//Arduino LIbrary
#include <FirebaseESP32.h>//Firebase library for ESP8266
#include <ESP_WiFiManager.h>//WiFi library
#include <Separador.h>//This library allows split a string

#define FIREBASE_HOST "medidor-de-potencia---internet-default-rtdb.firebaseio.com"//FB URL
#define FIREBASE_AUTH "CifUg0fUEbqdS0aBksAnJpjTP3StKniVlOLnLNx0"//FB Secret

void readserial_separates_sendtoFB();//Function to process the incoming serial data

FirebaseData fbdo;//FB object
Separador s;//Separator object

String inputString;
void setup() {
  
  Serial2.begin(9600);
  Serial.begin(9600);
  //Serial2.setRxBufferSize(128);
  //pinMode(LED_BUILTIN, OUTPUT);//ESP builtin Led declaration as output
  //digitalWrite(LED_BUILTIN, LOW);//Turn on the Led with a low (it works this way)

  WiFi.mode(WIFI_STA);// WiFi mode to autogenrate a network if the SSID and PW has changed
  ESP_WiFiManager wm;// Wifi Object
  wm.autoConnect("AutoConnectAP");//Conection to Wifi. If there's no newtwork information in the ESP
  //or if maybe the PW has changed, the ESP creates a LAN wich allows you configure the new parameters
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);//Conection to FB
}


void loop() {
 
//Main Loop: We only start the Serial port if the k counter is equals to 0, then we set it to 1 to assure
//that the program doesn't start the serial again. Then we call the SerialEvent function if there is
//something in the serial port.

readserial_separates_sendtoFB();

}  

void readserial_separates_sendtoFB(){
  
  if (Serial2.available()>0) Serial2.flush();
  while(Serial2.available()>0) inputString=Serial2.readString();
  String mediciones=s.separa(inputString,'S',2);
  Firebase.setString(fbdo,"Mediciones",mediciones);
  
  if (Serial2.available()>0) Serial2.flush();
  while(Serial2.available()>0) inputString=Serial2.readString();
  String M_YY=s.separa(inputString,'S',1);  
  String potencia_dias=s.separa(inputString,'S',3);
  String mes=s.separa(M_YY,'_',0);
  String anio=s.separa(M_YY,'_',1);
  String path_mes_anio= anio+"/"+mes;
  Firebase.setString(fbdo,path_mes_anio,potencia_dias);
/*
  //Separates the individual measures
  if(Serial2.available()>0) Serial2.flush();
  if(Serial2.available()>0) inputString=Serial2.readString();
  //String cosabien=s.separa(inputString,'T',0);
  String mediciones=s.separa(inputString,'S',1);//Separates the measures from the whole message
  String corriente=s.separa(mediciones,',',0);
  Firebase.setString(fbdo,"Mediciones/00Corriente",corriente);

  if(Serial2.available()>0) Serial2.flush();
  if(Serial2.available()>0) inputString=Serial2.readString();
  //cosabien=s.separa(inputString,'T',0);
  mediciones=s.separa(inputString,'S',1);
  String tension=s.separa(mediciones,',',1);
  Firebase.setString(fbdo,"Mediciones/01Voltaje",tension);

  if(Serial2.available()>0) Serial2.flush();
  if(Serial2.available()>0) inputString=Serial2.readString();
  //cosabien=s.separa(inputString,'T',0);
  mediciones=s.separa(inputString,'S',1);
  String fp_pmed_pad=s.separa(mediciones,',',2);
  Firebase.setString(fbdo,"Mediciones/02Factor de potencia",fp_pmed_pad);
  
  //Separates all the days
  if(Serial2.available()>0) Serial2.flush();
  if(Serial2.available()>0) inputString=Serial2.readString();
  String dias=s.separa(inputString,'S',2);
  Firebase.setString(fbdo,"Enero/Dias",dias);
  //Separates the individual days and the sends it to FB
  
  String dia1 = s.separa(dias,',',0);
  Firebase.setString(fbdo,"Dias/Dia01",dia1);
  
  String dia2 = s.separa(dias,',',1);
  Firebase.setString(fbdo,"Dias/Dia02",dia2);
  
  String dia3 = s.separa(dias,',',2);
  Firebase.setString(fbdo,"Dias/Dia03",dia3);
  
  String dia4 = s.separa(dias,',',3);
  Firebase.setString(fbdo,"Dias/Dia04",dia4);
  
  String dia5 = s.separa(dias,',',4);
  Firebase.setString(fbdo,"Dias/Dia05",dia5);
  
  String dia6 = s.separa(dias,',',5);
  Firebase.setString(fbdo,"Dias/Dia06",dia6);
  
  String dia7 = s.separa(dias,',',6);
  Firebase.setString(fbdo,"Dias/Dia07",dia7);
  
  String dia8 = s.separa(dias,',',7);
  Firebase.setString(fbdo,"Dias/Dia08",dia8);
  
  String dia9 = s.separa(dias,',',8);
  Firebase.setString(fbdo,"Dias/Dia09",dia9);

  if(Serial2.available()>0) Serial2.flush();
  if(Serial2.available()>0) inputString=Serial2.readString();
  //cosabien=s.separa(inputString,'T',0);
  String dias=s.separa(inputString,'S',2);
  String dia10 = s.separa(dias,',',9);
  Firebase.setString(fbdo,"Dias/Dia10",dia10);
  
  String dia11 = s.separa(dias,',',10);
  Firebase.setString(fbdo,"Dias/Dia11",dia11);
  
  String dia12 = s.separa(dias,',',11);
  Firebase.setString(fbdo,"Dias/Dia12",dia12);
  
  String dia13 = s.separa(dias,',',12);
  Firebase.setString(fbdo,"Dias/Dia13",dia13);
  
  String dia14 = s.separa(dias,',',13);
  Firebase.setString(fbdo,"Dias/Dia14",dia14);
  
  String dia15 = s.separa(dias,',',14);
  Firebase.setString(fbdo,"Dias/Dia15",dia15);
  
  String dia16 = s.separa(dias,',',15);
  Firebase.setString(fbdo,"Dias/Dia16",dia16);
  
  String dia17 = s.separa(dias,',',16);
  Firebase.setString(fbdo,"Dias/Dia17",dia17);
  
  String dia18 = s.separa(dias,',',17);
  Firebase.setString(fbdo,"Dias/Dia18",dia18);
  
  String dia19 = s.separa(dias,',',18);
  Firebase.setString(fbdo,"Dias/Dia19",dia19);
  
  String dia20 = s.separa(dias,',',19);
  Firebase.setString(fbdo,"Dias/Dia20",dia20);
  */
  }
