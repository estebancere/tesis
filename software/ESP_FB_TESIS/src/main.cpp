#include <Arduino.h>//Arduino LIbrary
#include <FirebaseESP8266.h>//Firebase library for ESP8266
#include <WiFiManager.h>//WiFi library
#include <Separador.h>//This library allows split a string

#define FIREBASE_HOST "medidor-de-potencia---internet-default-rtdb.firebaseio.com"//FB URL
#define FIREBASE_AUTH "CifUg0fUEbqdS0aBksAnJpjTP3StKniVlOLnLNx0"//FB Secret

void readserial_separates_sendtoFB();//Function to process the incoming serial data

FirebaseData fbdo;//FB object
Separador s;//Separator object
String inputString;
String id_medidor=WiFi.macAddress();
void setup() {
  
  Serial.begin(9600);
  
  pinMode(LED_BUILTIN, OUTPUT);//ESP builtin Led declaration as output
  digitalWrite(LED_BUILTIN, LOW);//Turn on the Led with a low (it works this way)

  WiFi.mode(WIFI_STA);// WiFi mode to autogenrate a network if the SSID and PW has changed
  WiFiManager wm;// Wifi Object
  wm.autoConnect("AutoConnectAP");//Conection to Wifi. If there's no newtwork information in the ESP
  //or if maybe the PW has changed, the ESP creates a LAN wich allows you configure the new parameters

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);//Conection to FB
  //Firebase.setString(fbdo,"ID_Cargador",id_cargador);
}


void loop() {
 
//Main Loop: We only start the Serial port if the k counter is equals to 0, then we set it to 1 to assure
//that the program doesn't start the serial again. Then we call the SerialEvent function if there is
//something in the serial port.

readserial_separates_sendtoFB();

}  

void readserial_separates_sendtoFB(){
  
  if (Serial.available()>0) Serial.flush();
  while(Serial.available()>0) inputString=Serial.readStringUntil('T');
  if (inputString[0]=='S'){
  Serial.println(inputString);
  String mediciones=s.separa(inputString,'S',2);
  Firebase.setString(fbdo,id_medidor+"/Mediciones",mediciones);
  }


  if (Serial.available()>0) Serial.flush();
  while(Serial.available()>0) inputString=Serial.readStringUntil('T');
  if (inputString[0]=='S'){
  String M_YY=s.separa(inputString,'S',1);  
  String potencia_dias=s.separa(inputString,'S',3);
  String mes=s.separa(M_YY,'_',0);
  if (mes[0]!='1') mes="0"+mes;
  String anio=s.separa(M_YY,'_',1);
  String path_mes_anio= "/20"+anio+"_"+mes;
  Firebase.setString(fbdo,id_medidor+path_mes_anio,potencia_dias);
  }
}